package com.realdolmen.chatclient;


public class ResponseMessage {
    private String author;
    private String message;
    private String timeDate;

    public ResponseMessage() {
    }

    public ResponseMessage(String author, String message, String timeDate) {
        this.author = author;
        this.message = message;
        this.timeDate = timeDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimeDate() {
        return timeDate;
    }

    public void setTimeDate(String timeDate) {
        this.timeDate = timeDate;
    }
}
