package com.realdolmen.chatclient;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.util.Scanner;
import java.util.concurrent.ExecutionException;

@SpringBootApplication
public class ChatClient {
    private static String URL = "ws://localhost:8080/websocket";

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        WebSocketClient client = new StandardWebSocketClient();
        WebSocketStompClient stompClient = new WebSocketStompClient(client);

        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        Scanner scanner = new Scanner(System.in);// Don't close immediately.

        System.out.print("Type in your name: ");
        MyStompSessionHandler myStompSessionHandler = new MyStompSessionHandler(scanner.nextLine());
        stompClient.connect(URL, myStompSessionHandler).get();

        while (scanner.hasNext()) {
            myStompSessionHandler.send(scanner.nextLine());
        }
    }
}
