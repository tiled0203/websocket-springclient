package com.realdolmen.chatclient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

import java.lang.reflect.Type;

public class MyStompSessionHandler extends StompSessionHandlerAdapter {
    private final String from;
    private Logger logger = LogManager.getLogger(MyStompSessionHandler.class);
    private StompSession session;

    public MyStompSessionHandler(String from) {
        this.from = from;
    }

    public void send(String message) {
        session.send("/app/request", new RequestMessage(this.from, message));
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        logger.info("New session established : " + session.getSessionId());
        this.session = session;
        session.subscribe("/topic/responses", this);
        logger.info("Subscribed to /topic/responses");
//        send("Howdy!");
        logger.info("Message sent to websocket server");
    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        logger.error("Got an exception", exception);
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return ResponseMessage.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        ResponseMessage msg = (ResponseMessage) payload;
        System.out.println("");
        logger.info("Received from " + msg.getAuthor() + ": " + msg.getMessage());
        System.out.print("Type message: ");
    }


}
